﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Сводное описание для Model
/// </summary>
namespace CScode
{
  public class Model
  {
    private readonly double g = 9.8;
    private readonly int numberOfDots = 1000;

    public Model()
    {
    }

    public Model(UAVstate initialState, UAVstate finiteState, int time)
    {
      InitialState = initialState;
      FiniteState = finiteState;
      Time = time;
      initialConditions = new double[3, 3]; // first number, second diff
      finiteConditions = new double[3, 3];
      initialU = new double[3];
      finiteU = new double[3];
      initialU[0] = InitialState.Nx;
      initialU[1] = InitialState.Ny * Math.Cos(initialState.Gamma);
      initialU[2] = InitialState.Ny * Math.Sin(initialState.Gamma);

      finiteU[0] = finiteState.Nx;
      finiteU[1] = finiteState.Ny * Math.Cos(initialState.Gamma);
      finiteU[2] = finiteState.Ny * Math.Sin(initialState.Gamma);

      coefficients = new double[6, 3];
      state = new double[9, numberOfDots];
      controls = new double[3, numberOfDots];

      InitializeConditions();
      findEquationCoefficients();
      ComputeStates();
      ComputeControls();
    }

    void InitializeConditions()
    {
      initialConditions[0, 0] = InitialState.H;
      initialConditions[1, 0] = InitialState.L;
      initialConditions[2, 0] = InitialState.Z;

      initialConditions[0, 1] = InitialState.V * Math.Sin(InitialState.Teta);
      initialConditions[1, 1] = InitialState.V * Math.Cos(InitialState.Teta) * Math.Cos(InitialState.Psi);
      initialConditions[2, 1] = -InitialState.V * Math.Cos(InitialState.Teta) * Math.Sin(InitialState.Psi);

      initialConditions[0, 2] = -g + initialU[0] * g * Math.Sin(InitialState.Teta) + initialU[1] * g * Math.Cos(InitialState.Teta);
      initialConditions[1, 2] = initialU[0] * g * Math.Cos(InitialState.Teta) * Math.Cos(InitialState.Psi) -
                                initialU[1] * g * Math.Sin(InitialState.Teta) * Math.Cos(InitialState.Psi) +
                                initialU[2] * g * Math.Sin(InitialState.Psi);
      initialConditions[2, 2] = -initialU[0] * g * Math.Cos(InitialState.Teta) * Math.Sin(InitialState.Psi) +
                                initialU[1] * g * Math.Sin(InitialState.Teta) * Math.Sin(InitialState.Psi) +
                                initialU[2] * g * Math.Cos(InitialState.Psi);

      finiteConditions[0, 0] = FiniteState.H;
      finiteConditions[1, 0] = FiniteState.L;
      finiteConditions[2, 0] = FiniteState.Z;

      finiteConditions[0, 1] = FiniteState.V * Math.Sin(FiniteState.Teta);
      finiteConditions[1, 1] = FiniteState.V * Math.Cos(FiniteState.Teta) * Math.Cos(FiniteState.Psi);
      finiteConditions[2, 1] = -FiniteState.V * Math.Cos(FiniteState.Teta) * Math.Sin(FiniteState.Psi);

      finiteConditions[0, 2] = -g + finiteU[0] * g * Math.Sin(FiniteState.Teta) + finiteU[1] * g * Math.Cos(FiniteState.Teta);
      finiteConditions[1, 2] = finiteU[0] * g * Math.Cos(FiniteState.Teta) * Math.Cos(FiniteState.Psi) -
                                 finiteU[1] * g * Math.Sin(FiniteState.Teta) * Math.Cos(FiniteState.Psi) +
                                 finiteU[2] * g * Math.Sin(FiniteState.Psi);
      finiteConditions[2, 2] = -finiteU[0] * g * Math.Cos(FiniteState.Teta) * Math.Sin(FiniteState.Psi) +
                                 finiteU[1] * g * Math.Sin(FiniteState.Teta) * Math.Sin(FiniteState.Psi) +
                                 finiteU[2] * g * Math.Cos(FiniteState.Psi);

    }

    void findEquationCoefficients()
    {
      for (int i = 0; i < 3; i++)
      {
        coefficients[0, i] = initialConditions[i, 0];
        coefficients[1, i] = initialConditions[i, 1] * Time;
        coefficients[2, i] = initialConditions[i, 2] * Time * Time * (0.5);
        coefficients[3, i] = finiteConditions[i, 0] * 10.0 - 10.0 * initialConditions[i, 0] - 6.0 * initialConditions[i, 1] * Time -
                             (1.5) * initialConditions[i, 2] * Time * Time - 4.0 * finiteConditions[i, 1] * Time +
                             (0.5) * finiteConditions[i, 2] * Time * Time;
        coefficients[4, i] = -15.0 * finiteConditions[i, 0] + 15.0 * initialConditions[i, 0] + 8.0 * initialConditions[i, 1] * Time +
                             (1.5) * initialConditions[i, 2] * Time * Time + 7.0 * finiteConditions[i, 1] * Time -
                             finiteConditions[i, 2] * Time * Time;
        coefficients[5, i] = 6.0 * finiteConditions[i, 0] - 6.0 * initialConditions[i, 0] - 3.0 * initialConditions[i, 1] * Time -
                             (0.5) * initialConditions[i, 2] * Time * Time - 3.0 * finiteConditions[i, 1] * Time +
                             (0.5) * finiteConditions[i, 2] * Time * Time;

      }
    }

    private double ComputePolynom(int i, double dot)
    {
      return coefficients[0, i] + coefficients[1, i] * dot + coefficients[2, i] * dot * dot + coefficients[3, i] * dot * dot * dot +
             coefficients[4, i] * dot * dot * dot * dot + coefficients[5, i] * dot * dot * dot * dot * dot;
    }
    private double ComputeDiffPolynom(int i, double dot)
    {
      return coefficients[1, i] + coefficients[2, i] * dot + coefficients[3, i] * dot * dot +
             coefficients[4, i] * dot * dot * dot + coefficients[5, i] * dot * dot * dot * dot;
    }
    private double ComputeDiff2Polynom(int i, double dot)
    {
      return coefficients[2, i] + coefficients[3, i] * dot +
             coefficients[4, i] * dot * dot + coefficients[5, i] * dot * dot * dot;
    }
    private void ComputeStates()
    {
      for (int i = 0; i < numberOfDots; ++i)
      {
        double dot = i * (1.0 / (double)(numberOfDots));
        for (int j = 0; j < 3; ++j)
        {
          state[j, i] = ComputePolynom(j, dot);
          state[j + 3, i] = ComputeDiffPolynom(j, dot) / Time;
          state[j + 6, i] = ComputeDiff2Polynom(j, dot) / (Time * Time);
        }
      }
    }

    private void ComputeControls()
    {
      double[] V = new double[numberOfDots];
      double[] sinTeta = new double[numberOfDots];
      double[] cosTeta = new double[numberOfDots];
      double[] sinPsi = new double[numberOfDots];
      double[] cosPsi = new double[numberOfDots];
      for (int i = 0; i < numberOfDots; ++i)
      {
        V[i] = Math.Sqrt(state[3, i] * state[3, i] + state[4, i] * state[4, i] + state[5, i] * state[5, i]);
        sinTeta[i] = state[3, i] / V[i];
        cosTeta[i] = Math.Sqrt(state[4, i] * state[4, i] + state[5, i] * state[5, i]) / V[i];
        sinPsi[i] = -state[5, i] / Math.Sqrt(state[4, i] * state[4, i] + state[5, i] * state[5, i]);
        cosPsi[i] = state[4, i] / Math.Sqrt(state[4, i] * state[4, i] + state[5, i] * state[5, i]);
      }
      for (int i = 0; i < numberOfDots; ++i)
      {
        controls[0, i] = sinTeta[i] +
                         (1.0 / g) *
                         (state[6, i] * sinTeta[i] + state[7, i] * cosTeta[i] * cosPsi[i] - state[7, i] * cosTeta[i] * sinPsi[i]);
        controls[1, i] = cosTeta[i] +
                         (1.0 / g) *
                         (state[6, i] * cosTeta[i] - state[7, i] * sinTeta[i] * cosPsi[i] - state[7, i] * sinTeta[i] * sinPsi[i]);
        controls[2, i] = (1.0 / g) *
                         (state[6, i] * sinPsi[i] - state[7, i] * cosPsi[i]);

      }
      for (int i = 0; i < numberOfDots; ++i)
      {
        controls[2, i] = Math.Atan(controls[2, i] / controls[1, i]);
        controls[1, i] = Math.Cos(controls[2, i]) / controls[1, i];
      }
    }

    public string createOutPut()
    {

      string ans = "";
      for (int i = 1; i < numberOfDots; ++i)
      {

        double dot = i * (Time / (double)(numberOfDots));
        ans += "[" + dot.ToString().Replace(",", ".") + "," + controls[0, i].ToString().Replace(",", ".") + "," + controls[1, i].ToString().Replace(",", ".") + "," + controls[2, i].ToString().Replace(",", ".") + "]";
        if (i + 1 != numberOfDots)
          ans = ans + ",";

      }

      return ans;
    }
    public double[,] getStates()
    {
      return state;
    }

    public int getNumberOfDots()
    {
      return numberOfDots;
    }

    private double[,] controls;
    private double[,] state;

    private double[,] coefficients;

    private double[] initialU;
    private double[] finiteU;
    private double[,] initialConditions;
    private double[,] finiteConditions;
    public UAVstate InitialState { get; set; }
    public UAVstate FiniteState { get; set; }

    public int Time { get; set; }
  }
}