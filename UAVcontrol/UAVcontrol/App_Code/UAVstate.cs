﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Сводное описание для UAVstate
/// </summary>
namespace CScode
{

 
public class UAVstate
{
  public UAVstate()
  {
  }

  public UAVstate(double v, double teta, double psi, double h, double l, double z, double nx, double ny, double gamma)
  {
    V = v;
    Teta = teta;
    Psi = Psi;
    H = h;
    L = l;
    Z = z;
    Nx = nx;
    Ny = ny;
    Gamma = gamma;
  }
  public void SetStates(double v, double teta, double psi, double h, double l, double z, double nx, double ny, double gamma)
  {
    V = v;
    Teta = teta;
    Psi = Psi;
    H = h;
    L = l;
    Z = z;
    Nx = nx;
    Ny = ny;
    Gamma = gamma;
  }
  public double V { get; set; }
  public double Teta { get; set; }
  public double Psi { get; set; }
  public double H { get; set; }
  public double L { get; set; }
  public double Z { get; set; }
  public double Nx { get; set; } 
  

 

  public double Ny { get; set; }
  public double Gamma { get; set; }

}
}